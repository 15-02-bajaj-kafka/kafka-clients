package com.classpath.bootkafka.config;

import com.classpath.bootkafka.model.Order;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    private final KafkaTemplate<String, String> factKafkaTemplate;

    public KafkaConsumerConfig(KafkaTemplate<String, String> factKafkaTemplate) {
        this.factKafkaTemplate = factKafkaTemplate;
    }

    @Bean
    public Map<String, Object> consumerConfigs(){
        HashMap<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.64.198:9092, 128.199.16.50:9092,157.245.98.120:9092");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }

    @Bean
    public ConsumerFactory<String, Order> orderConsumerFactory(){
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(),
                                                new StringDeserializer(),
                                                new JsonDeserializer<>(Order.class));
    }
    @Bean
    public ConsumerFactory<String, String> factConsumerFactory(){
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(),
                                                new StringDeserializer(),
                                                new StringDeserializer());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Order> orderPriceLessThan4kContainerFactory(){
        ConcurrentKafkaListenerContainerFactory<String, Order> concurrentKafkaListenerContainerFactory = new ConcurrentKafkaListenerContainerFactory<>();
        concurrentKafkaListenerContainerFactory.setConsumerFactory(orderConsumerFactory());
        concurrentKafkaListenerContainerFactory.setRecordFilterStrategy(orderRecord ->  orderRecord.value().getPrice() > 4000);
        return concurrentKafkaListenerContainerFactory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> factsContainerFactory(){
        ConcurrentKafkaListenerContainerFactory<String, String> concurrentKafkaListenerContainerFactory = new ConcurrentKafkaListenerContainerFactory<>();
        concurrentKafkaListenerContainerFactory.setConsumerFactory(factConsumerFactory());
        concurrentKafkaListenerContainerFactory.setReplyTemplate(factKafkaTemplate);
        return concurrentKafkaListenerContainerFactory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Order> orderPriceMoreThan4kContainerFactory(){
        ConcurrentKafkaListenerContainerFactory<String, Order> concurrentKafkaListenerContainerFactory = new ConcurrentKafkaListenerContainerFactory<>();
        concurrentKafkaListenerContainerFactory.setConsumerFactory(orderConsumerFactory());
        concurrentKafkaListenerContainerFactory.setRecordFilterStrategy(orderRecord ->  orderRecord.value().getPrice() <= 4000);
        return concurrentKafkaListenerContainerFactory;
    }

}
