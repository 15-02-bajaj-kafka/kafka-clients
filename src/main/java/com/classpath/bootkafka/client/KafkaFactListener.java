package com.classpath.bootkafka.client;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaResourceHolder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;

@Configuration
public class KafkaFactListener {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public KafkaFactListener(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @KafkaListener(containerFactory = "factsContainerFactory",
                   groupId = "app-1",
                   topics = "pradeep-facts-input")
    //@SendTo("facts-uppercase")
    public void consumer1(@Payload String fact,
                                   @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.OFFSET) int offset){
        System.out.println("Consumer - 1 ");
        System.out.print(" Partition :: "+ partition +"\t");
        System.out.print(" Offset :: "+ offset);
    }
    @KafkaListener(containerFactory = "factsContainerFactory",
                   groupId = "app-1",
                   topics = "pradeep-facts-input")
    //@SendTo("facts-uppercase")
    public void consumer2(@Payload String fact,
                                   @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.OFFSET) int offset){
        System.out.println("Consumer - 2 ");
        System.out.print(" Partition :: "+ partition +"\t");
        System.out.print(" Offset :: "+ offset);
    }

    @KafkaListener(containerFactory = "factsContainerFactory",
                   groupId = "app-1",
                   topics = "pradeep-facts-topic")
    //@SendTo("facts-uppercase")
    public void consumer3(@Payload String fact,
                                   @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.OFFSET) int offset){
        System.out.println("Consumer - 3 ");
        System.out.print(" Partition :: "+ partition +"\t");
        System.out.print(" Offset :: "+ offset);
    }
    @KafkaListener(containerFactory = "factsContainerFactory",
                   groupId = "app-1",
                   topics = "pradeep-facts-topic")
    //@SendTo("facts-uppercase")
    public void consumer4(@Payload String fact,
                                   @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.OFFSET) int offset){
        System.out.println("Consumer - 4 ");
        System.out.print(" Payload :: "+ fact + "\t");
        System.out.print(" Offset :: "+ offset);
    }
    @KafkaListener(containerFactory = "factsContainerFactory",
                   groupId = "app-1",
                   topics = "pradeep-facts-topic")
    //@SendTo("facts-uppercase")
    public void consumer5(@Payload String fact,
                                   @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.OFFSET) int offset){
        System.out.println("Consumer - 5 ");
        System.out.print(" Partition :: "+ partition +"\t");
        System.out.print(" Offset :: "+ offset);
    }
}
