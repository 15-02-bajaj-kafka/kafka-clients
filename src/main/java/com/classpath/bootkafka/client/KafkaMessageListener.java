package com.classpath.bootkafka.client;

import com.classpath.bootkafka.model.Order;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaMessageListener {

   private final KafkaTemplate<String, Order> kafkaTemplate;

    public KafkaMessageListener(KafkaTemplate<String, Order> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }
/*
    @KafkaListener(topics = "day-3-spring-boot-pradeep" ,groupId = "app-group")
    public void processMessages(Order order){
        System.out.println("===============Inside the kafka listener ====:: "+ order);
        if(order.getPrice() < 4000){
            this.kafkaTemplate.send("order-less-than-four-thousand", order);
        }else {
            this.kafkaTemplate.send("order-more-than-four-thousand", order);
        }
    }*/

/*
    @KafkaListener(
            topics = "day-3-spring-boot-orders",
            containerFactory = "orderPriceLessThan4kContainerFactory",
            groupId = "app-consumer-less"
            )
*/
    public void processOrder(Order order){
        System.out.println("===============Inside the kafka listener with Order price LESS than 4k ====:: "+ order);
        System.out.println(order);
        System.out.println("===============Inside the kafka listener with Order price LESS than 4k ====:: "+ order);
    }
    /*@KafkaListener(
            topics = "day-3-spring-boot-orders",
            containerFactory = "orderPriceMoreThan4kContainerFactory",
            groupId = "app-consumer-more"
            )*/
    public void processOrderMoreThan4k(Order order){
        System.out.println("===============Inside the kafka listener with Order price MORE than 4k ====:: "+ order);
        System.out.println(order);
        System.out.println("===============Inside the kafka listener with Order price MORE than 4k ====:: "+ order);
    }

}
