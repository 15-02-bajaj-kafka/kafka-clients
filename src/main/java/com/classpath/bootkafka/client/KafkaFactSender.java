package com.classpath.bootkafka.client;

import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
public class KafkaFactSender implements CommandLineRunner {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public KafkaFactSender(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
       /* Faker faker = new Faker();
        IntStream.range(1, 100).forEach(index -> {
            String message = faker.chuckNorris().fact().toLowerCase();
            kafkaTemplate.send("facts-input", message);
            System.out.println("Producer sent the message :: "+ message);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread.sleep(2000);*/
    }
}
