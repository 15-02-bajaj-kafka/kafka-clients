package com.classpath.bootkafka.client;

import com.classpath.bootkafka.model.Order;
import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.IntStream;

@Component
public class KafkaMessageSender implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    private final KafkaTemplate<String,Order> kafkaTemplate;

    public KafkaMessageSender(ApplicationContext applicationContext, KafkaTemplate<String, Order> kafkaTemplate) {
        this.applicationContext = applicationContext;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("=============Inside the main method==================");
        Faker faker = new Faker();
       /* Arrays
                .stream(this.applicationContext
                .getBeanDefinitionNames())
                .limit(1)
                .forEach(bean -> {
                    ListenableFuture<SendResult<String, String>> listenableFuture = this.kafkaTemplate.send("day-3-spring-boot-pradeep", bean);
                    listenableFuture.addCallback((response) -> {
                        System.out.println("Inside the success response");
                        RecordMetadata recordMetadata = response.getRecordMetadata();
                        System.out.println("Offset: "+ recordMetadata.offset());
                        System.out.println("partition: " +recordMetadata.partition());
                        System.out.println("Record timestamp: " +recordMetadata.timestamp());
                    }, (res) -> {
                        String message = res.getMessage();
                        System.out.println("Error message: "+message);
                    });
                });*/

       /* IntStream.range(0, 100)
                .forEach(index -> {
                    try {
                        Thread.sleep(2000);

                    Order order = Order.builder()
                                            .id(faker.number().numberBetween(4000L, 8000L))
                                        .price(faker.number().randomDouble(2, 3000, 5000))
                                        .customerName(faker.name().firstName())
                                        .build();
                    this.kafkaTemplate.send("day-3-spring-boot-orders", order);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });*/
    }
}
